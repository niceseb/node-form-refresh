/**
 * Created by minminsanjose on 11/08/2015.
 */
var mongoose = require('mongoose');
var user = require('./../database');
///node-mongoose-form/test/test.js
///node-mongoose-form/database.js

// Connecting to a local test database or creating it on the fly
mongoose.connect('mongodb://localhost/nodetest-test');

/*
 * Mocha Test
 *
 * Tests are organized by having a "describe" and "it" method. Describe
 * basically creates a "section" that you are testing and the "it" method
 * is what runs your test code.
 *
 * For asynchronous tests you need to have a done method that you call after
 * your code should be done executing so Mocha runs to test properly.
 */

describe('Users', function () {
    var currentUser = null;

    /*
     * beforeEach Method
     *
     * The before each method will execute every time Mocha is run. This
     * code will not run every time an individual test is run.
     */

    beforeEach(function (done) {
        user.register('Sebastian Cheung', 46, "Male", "UK", Date.now(), function (doc) {
            currentUser = doc;
            done();
        });
    });

    /*
    * afterEach Method
    *
    * Just like the beforeEach, afterEach is run after Mocha has completed
    * running it's queue.
    */

    afterEach(function (done) {
        user.model.remove({}, function () {
            done();
     });
    });

    it('registers a new user', function (done) {
        user.register('Sebastian Cheung', 46, "Male", "UK", Date.now(), function (doc) {
            doc.name.should.eql('Sebastian Cheung');
            done();
        });
    });

    it('fetches user by name', function (done) {
        user.findByName('Sebastian Cheung', function (doc) {
            doc.name.should.eql('Sebastian Cheung');
            done();
        });
    });

    it('fetches user by age', function (done) {
        user.findByAge(46, function (doc) {
            doc.age.should.eql(46);
            done();
        });
    });

    it('fetches user by gender', function (done) {
        user.findByGender("Male", function (doc) {
            doc.sex.should.eql("Male");
            done();
        });
    });

    it('fetches user by country', function (done) {
        user.findByCountry("UK", function (doc) {
            doc.country.should.eql("UK");
            done();
        });
    });


});