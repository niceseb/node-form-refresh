/*
 * User Model
 *
 * Instead of following the traditional Mongoose examples, I'm
 * using a function to provide both private and public methods to
 * this model to keep things more organized!
 */
var User = function () {
    var mongoose = require('mongoose');
    var Schema   = mongoose.Schema;
    var Comment = new Schema({
    //name : { type: String, index: { unique: true, required: true}},
    name : String,
    age : Number,
    sex : String,
    country : String,
    dateCreated : Date
    });

    // Declaring a private model for internal methods
    var _model = mongoose.model('userdata', Comment); //was comments

    // Creating a register method for convenience
    var _register = function (name, age, sex, country, dateCreated, callback) {
        _model.create({name: name, age: age, sex: sex, country: country, dateCreated: dateCreated}, function (e, doc) {
            if (e) {
                fail(e);
            } else {
                callback(doc);
            }
        });
    };

    // Creating a findByName method for convenience
    var _findByName = function (name, success, fail) {
        _model.findOne({name: name}, function (e, doc) {
            if(e) {
                fail(e);
            } else {
                success(doc);
            }
        });
    };

    // Creating a findByAge method for convenience
    var _findByAge = function (age, success, fail) {
        _model.findOne({age: age}, function (e, doc) {
            if(e) {
                fail(e);
            } else {
                success(doc);
            }
        });
    };

    // Creating a findBySex method for convenience
    var _findByGender = function (sex, success, fail) {
        _model.findOne({sex: sex}, function (e, doc) {
            if(e) {
                fail(e);
            } else {
                success(doc);
            }
        });
    };

    // Creating a findByCountry method for convenience
    var _findByCountry = function (country, success, fail) {
        _model.findOne({country: country}, function (e, doc) {
            if(e) {
                fail(e);
            } else {
                success(doc);
            }
        });
    };

    // Creating a findByDateCreated method for convenience
    var _findByDateCreated = function (dateCreated, success, fail) {
        _model.findOne({dateCreated: dateCreated}, function (e, doc) {
            if(e) {
                fail(e);
            } else {
                success(doc);
            }
        });
    };

    // Returning properties and methods we'd like to be public
    return {
        register: _register,
        schema: Comment,
        model: _model,
        findByName: _findByName,
        findByAge: _findByAge,
        findByGender: _findByGender,
        findByCountry: _findByCountry,
        findByDateCreated: _findByDateCreated
    }
}();

module.exports = User;

//Move this to somewhere
//mongoose.connect('mongodb://localhost/nodetest'); //was node-comment