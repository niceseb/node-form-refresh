var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var Comment = mongoose.model('userdata'); //was comments

// Connecting to a local test database or creating it on the fly
mongoose.connect('mongodb://localhost/nodetest');

/* GET form. */
router.get('/', function(req, res) {
  Comment.find(function(err, userdata){
    console.log(userdata);
    res.render(
      'form',
      {title : 'Some Useful Government Service'}
    );
  });
});

/* POST form. */
router.post('/', function(req, res) {

  req.assert('comment', 'Name is required').notEmpty();
  req.assert('age', 'Age is required').notEmpty().isInt();
  req.assert('country', 'Country is required').notEmpty();

  var errors = req.validationErrors();
  if (errors)
    res.render('form', {errors: errors});
  else {
    user = req.body.comment;
    new Comment({name: req.body.comment, sex: req.body.optradio, age: req.body.age, country: req.body.country, dateCreated: Date.now()})
        .save(function (err, userdata) {
          console.log(userdata);
          res.redirect('users'); //form users
        });
  }
});

module.exports = router;