// mongoose config
require('./database');

var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var cheerio = require('cheerio');

//var check = require('validator').check,
//    sanitize = require('validator').sanitize;

var routes = require('./routes/index');
var users = require('./routes/users');
var form = require('./routes/form');


var expressValidator = require('express-validator'); //slow?
//var mongoose = require('mongoose');
var config = require('./config');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

//http://stackoverflow.com/questions/29694647/app-configurefunction-typeerror-undefined-is-not-a-function
//http://expressjs.com/guide/migrating-4.html#other-changes
//app.configure(function () {
//    //set the 'dbUrl' to the mongodb url that corresponds to
//    //the environment we are in
//    app.set('dbUrl', config.db[app.settings.env]);
//    //connect mongoose to the mongo dbUrl
//    mongoose.connect(app.get('dbUrl'));
//});

//if ('development' == app.get('env')) {
//      //set the 'dbUrl' to the mongodb url that corresponds to
//      //the environment we are in
//      app.set('dbUrl', config.db[app.settings.env]);
//      //connect mongoose to the mongo dbUrl
//      mongoose.connect(app.get('dbUrl'));
//}



// uncomment after placing your favicon in /public
//app.use(favicon(__dirname + '/public/favicon.ico'));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(expressValidator()); // this line must be immediately after express.bodyParser

app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', routes);
app.use('/users', users);
app.use('/form', form);
app.use('/create', form);



app.locals.europedata = require('./europe.json');
app.locals.user = "default";

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});


console.log("Current mode is : "+ app.settings.env);

module.exports = app;
